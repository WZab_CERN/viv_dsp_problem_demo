library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.fixed_float_types.all;
library work;
use work.my_fixed_pkg.all;
use work.dsp_demo_pkg.all;

entity dsp_demo is

  port (
    clk_in : in  std_logic;
    rst    : in  std_logic;
    din1   : in  t_data1;
    din2   : in  t_data1;
    din3   : in  t_data1;
    dout   : out t_data1;
    ctl1   : in  std_logic;
    ctl2   : in  std_logic
    );

end dsp_demo;

architecture beh1 of dsp_demo is

  attribute use_dsp           : string;
  attribute srl_style         : string;
  attribute use_dsp of beh1   : architecture is "yes";
  attribute IOB               : string;
  attribute retiming_forward  : integer;
  attribute retiming_backward : integer;

  constant del_in  : integer := 6;
  constant del_out : integer := 6;

  signal clk : std_logic := '0';

  -- Signals introduced to be able to specify IOB in the HDL code.
  signal din1_first : t_data1   := to_data1(0.0);
  signal din2_first : t_data1   := to_data1(0.0);
  signal din3_first : t_data1   := to_data1(0.0);
  signal ctl1_first : std_logic := '0';
  signal ctl2_first : std_logic := '0';

  attribute IOB of din1_first : signal is "TRUE";
  attribute IOB of din2_first : signal is "TRUE";
  attribute IOB of din3_first : signal is "TRUE";
  attribute IOB of ctl1_first : signal is "TRUE";
  attribute IOB of ctl2_first : signal is "TRUE";

  signal din1_del : t_data1_vec(del_in-1 downto 0)       := (others => to_data1(0.0));
  signal din2_del : t_data1_vec(del_in-1 downto 0)       := (others => to_data1(0.0));
  signal din3_del : t_data1_vec(del_in-1 downto 0)       := (others => to_data1(0.0));
  signal ctl1_del : std_logic_vector(del_out-1 downto 0) := (others => '0');
  signal ctl2_del : std_logic_vector(del_out-1 downto 0) := (others => '0');

  attribute retiming_forward of din1_del, din2_del, din3_del : signal is 1;

  signal dout_del : t_data1_vec(del_out-1 downto 0) := (others => to_data1(0.0));

  -- Signal introduced to be able to specify IOB in the HDL code.
  signal dout_last                                              : t_data1 := to_data1(0.0);
  attribute IOB of dout_last                                    : signal is "TRUE";
  attribute srl_style of din1_del, din2_del, din3_del, dout_del : signal is "register";

  signal dout_x, dout_x1, dout_x2, dout_x3 : t_data1 := to_data1(0.0);

  attribute retiming_backward of dout_del, dout_x, dout_x1, dout_x2, dout_x3 : signal is 1;

  component clk_wiz_0
    port
      (
        clk_out1 : out std_logic;
        clk_in1  : in  std_logic
        );
  end component;

  component calc is
    port (
      ap_clk    : in  std_logic;
      ap_rst    : in  std_logic;
      din1_V    : in  std_logic_vector (13 downto 0);
      din2_V    : in  std_logic_vector (13 downto 0);
      din3_V    : in  std_logic_vector (13 downto 0);
      ctl1_V    : in  std_logic_vector (0 downto 0);
      ctl2_V    : in  std_logic_vector (0 downto 0);
      ap_return : out std_logic_vector (13 downto 0));
  end component calc;

  signal din1_V    : std_logic_vector (13 downto 0);
  signal din2_V    : std_logic_vector (13 downto 0);
  signal din3_V    : std_logic_vector (13 downto 0);
  signal ctl1_V    : std_logic_vector (0 downto 0);
  signal ctl2_V    : std_logic_vector (0 downto 0);
  signal ap_return : std_logic_vector (13 downto 0);

  signal ap_rst : std_logic := '0';

begin

  clkwiz : clk_wiz_0
    port map (
      -- Clock out ports
      clk_out1 => clk,
      -- Clock in ports
      clk_in1  => clk_in
      );

  calc_1 : entity work.calc
    port map (
      ap_clk    => clk,
      ap_rst    => rst,
      din1_V    => din1_V,
      din2_V    => din2_V,
      din3_V    => din3_V,
      ctl1_V    => ctl1_V,
      ctl2_V    => ctl2_V,
      ap_return => ap_return);

  process(all) is
  begin  -- process
    for i in 13 downto 0 loop
      din1_V(i)      <= din1_first(i-5);
      din2_V(i)      <= din2_first(i-5);
      din3_V(i)      <= din3_first(i-5);
      dout_last(i-5) <= ap_return(i);
    end loop;  -- i
    ctl1_V(0) <= ctl1;
    ctl2_V(0) <= ctl2;
  end process;


  p2 : process (clk) is
  begin  -- process p2
    if clk'event and clk = '1' then     -- rising clock edge
      din1_first <= din1;
      din2_first <= din2;
      din3_first <= din3;
      dout       <= dout_last;
    end if;
  end process p2;

end architecture;
