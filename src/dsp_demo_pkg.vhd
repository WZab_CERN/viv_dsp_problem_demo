library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.fixed_float_types.all;
use work.my_fixed_pkg.all;

package dsp_demo_pkg is

  subtype t_data1 is sfixed(8 downto -5);
  type t_data1_vec is array (natural range <>) of t_data1;

  function to_data1 (
    constant vin : real)
    return t_data1;

  function resize_to_data1 (
    constant vin : sfixed)
    return t_data1;

end package dsp_demo_pkg;

package body dsp_demo_pkg is

  function to_data1 (
    constant vin : real)
    return t_data1 is
    variable res : t_data1;
  begin  -- function to_t_data1
    res := to_sfixed(vin, 8, -5);
    return res;
  end function to_data1;

  function resize_to_data1 (
    constant vin : sfixed)
    return t_data1 is
    variable res : t_data1;
  begin  -- function to_t_data1
    res := resize(vin, 8, -5);
    return res;
  end function resize_to_data1;


end package body dsp_demo_pkg;
