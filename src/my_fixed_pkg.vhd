library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.fixed_float_types.all;
use ieee.fixed_pkg.all;

package my_fixed_pkg is
  new ieee.fixed_generic_pkg
    generic map (
-- Uncomment one of the below settings to select the rounding method
-- Round, do not truncate
--    fixed_round_style    => ieee.fixed_float_types.fixed_round,
-- Truncate, don’t round
     fixed_round_style    => ieee.fixed_float_types.fixed_truncate,
-- Uncomment one of the below settings to select the overflow handling
-- Saturate, don’t wrap
     fixed_overflow_style => ieee.fixed_float_types.fixed_saturate,
-- Wrap, don’t saturate
--      fixed_overflow_style => ieee.fixed_float_types.fixed_wrap,

      fixed_guard_bits     => 1,        -- Don’t need the extra guard bits
      no_warning           => true      -- turn warnings off
      );
