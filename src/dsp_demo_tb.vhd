-------------------------------------------------------------------------------
-- Title      : Testbench for design "dsp_demo"
-- Project    : 
-------------------------------------------------------------------------------
-- File       : dsp_demo_tb.vhd
-- Author     : FPGA Developer  <xl@WZabHP.nasz.dom>
-- Company    : 
-- Created    : 2023-11-24
-- Last update: 2023-11-25
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2023 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2023-11-24  1.0      xl      Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.fixed_float_types.all;

library work;
use work.my_fixed_pkg.all;
use work.dsp_demo_pkg.all;
-------------------------------------------------------------------------------

entity dsp_demo_tb is

end entity dsp_demo_tb;

-------------------------------------------------------------------------------

architecture sim of dsp_demo_tb is

  -- component ports
  signal din1 : t_data1   := to_data1(0.0);
  signal din2 : t_data1   := to_data1(0.0);
  signal din3 : t_data1   := to_data1(0.0);
  signal dout : t_data1   := to_data1(0.0);
  signal ctl1 : std_logic := '0';
  signal ctl2 : std_logic := '0';

  -- clock
  signal clk : std_logic := '1';
  signal rst : std_logic := '1';

begin  -- architecture sim

  -- component instantiation
  DUT : entity work.dsp_demo
    port map (
      clk_in => clk,
      rst    => rst,
      din1   => din1,
      din2   => din2,
      din3   => din3,
      dout   => dout,
      ctl1   => ctl1,
      ctl2   => ctl2);

  -- clock generation
  Clk <= not Clk after 10 ns;

  -- waveform generation
  WaveGen_Proc : process
  begin
    -- insert signal assignments here

    wait until Clk = '1';
    din1 <= to_data1(1.23);
    din2 <= to_data1(2.34);
    din3 <= to_data1(5.11);
    ctl1 <= '0';
    ctl2 <= '0';
    wait until Clk = '1';
    wait until Clk = '0';
    ctl1 <= '1';
    ctl2 <= '0';
    wait until Clk = '1';
    wait until Clk = '0';
    ctl1 <= '1';
    ctl2 <= '1';
    wait until Clk = '1';
    wait until Clk = '0';
    ctl1 <= '0';
    ctl2 <= '1';
    wait until Clk = '1';
    wait until Clk = '0';
    wait;
  end process WaveGen_Proc;



end architecture sim;

-------------------------------------------------------------------------------

configuration dsp_demo_tb_sim_cfg of dsp_demo_tb is
  for sim
  end for;
end dsp_demo_tb_sim_cfg;

-------------------------------------------------------------------------------
