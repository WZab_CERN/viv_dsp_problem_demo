library ieee;
use ieee.std_logic_1164.all;
library work;


entity clk_wiz_0 is

  port (
    clk_in1 : in  std_logic;
    clk_out1: out std_logic
    );

end clk_wiz_0;

architecture beh1 of clk_wiz_0 is


begin

  clk_out1 <= clk_in1;
  
end architecture;
