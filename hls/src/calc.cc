#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string.h>
#include <ap_int.h>

typedef ap_fixed<14,9,AP_TRN,AP_SAT> mydata;

mydata calc(mydata din1, mydata din2, mydata din3, ap_int<1> ctl1, ap_int<1> ctl2)
{
#pragma HLS interface ap_none port=din1
#pragma HLS interface ap_none port=din2
#pragma HLS interface ap_none port=din3
#pragma HLS interface ap_none port=ctl1
#pragma HLS interface ap_none port=ctl2
#pragma HLS interface ap_ctrl_none port=return
#pragma HLS PIPELINE II=1
  if(ctl1 == 0) {
    return (din1 + din2) * din3;
  } else {
    if(ctl2 == 0) {
      return din1 + din2 + din3;
    } else {
      return din1 * (din2 - din3);
    }
  }
}
