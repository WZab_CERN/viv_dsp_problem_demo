############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2020 Xilinx, Inc. All Rights Reserved.
############################################################
open_project calc
set_top calc
add_files src/calc.cc
open_solution "solution1"
set_part {xc7vx330t-ffg1157-3}
create_clock -period 4 -name default
config_export -format ip_catalog -rtl vhdl
set_clock_uncertainty 0.010
#source "./calc/solution1/directives.tcl"
#csim_design
csynth_design
#cosim_design
export_design -rtl vhdl -format ip_catalog
