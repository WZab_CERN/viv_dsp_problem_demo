STD=synopsys
VSTD=08
ENTITY=dsp_demo_tb
#RUN_OPTIONS= --stop-time=2900ns --wave=${ENTITY}.ghw 
RUN_OPTIONS= --stop-time=2900ns 
#RUN_OPTIONS=  --wave=${ENTITY}.ghw 
SOURCES = \
  src/hls/calc_mul_mul_15s_bkb.vhd \
  src/hls/calc_am_submul_14cud.vhd \
  src/hls/calc.vhd \
  src/clk_wiz_0.vhd \
  src/my_fixed_pkg.vhd \
  src/dsp_demo_pkg.vhd \
  src/dsp_demo.vhd \
  src/dsp_demo_tb.vhd \

OBJECTS=$(SOURCES:.vhd=.o)

all: $(OBJECTS)

%.o : %.vhd
	ghdl -a -g -C  --std=${VSTD} --ieee=${STD} $<
#	ghdl -a -g -C --workdir=comp --std=${VSTD} --ieee=${STD} $<

#--trace-signals --trace-processes
#RUN_OPTIONS= 
#--trace-processes
all: reader
reader:   ${ENTITY} ${ENTITY}.ghw
	gtkwave ${ENTITY}.ghw ${ENTITY}.sav
${ENTITY}: $(SOURCES:.vhd=.o)
#	vhdlp -work fmf fmf/*.vhd
#	ghdl -e -g --mb-comments --workdir=comp --std=${VSTD} -fexplicit --ieee=${STD} ${ENTITY}
	ghdl -e -g --mb-comments  --std=${VSTD} -fexplicit --ieee=${STD} ${ENTITY}
${ENTITY}.ghw: ${ENTITY}
#	./${ENTITY} --wave=${ENTITY}.ghw  ${RUN_OPTIONS} --stop-time=50000ns 2>&1 > res.txt
	./${ENTITY} --wave=${ENTITY}.ghw ${RUN_OPTIONS} 
#> res.txt  2>&1 
clean:
	rm -f comp/* *.o *.vcd *.cf *.ghw events* ${ENTITY}

